/* vim: set et ts=4 sw=4: */

#include "presenterpdf.h"

PresenterPdf::PresenterPdf(PDFModel *modele, Parameters *params)
{
    this->modele = modele;
    this->params = params;

    this->setWindowTitle("PresenterPdf");
    this->imgLabel = new SlideWidget(this, modele, params);
    this->imgLabel->setAlignment(Qt::AlignCenter);
    this->setCentralWidget(this->imgLabel);
    this->setStyleSheet("background-color: black;");

    QObject::connect(this->modele, SIGNAL(renderingChanged()), SLOT(updateView()));
    QObject::connect(this, SIGNAL(keyPressed(QKeyEvent*)),
                     this->modele, SLOT(handleKeyModelSequence(QKeyEvent*)));
    QObject::connect(this->params, SIGNAL(projectorScreenChanged()), SLOT(moveToScreen()));

    this->moveToScreen();
}

void PresenterPdf::moveToScreen()
{
    this->showNormal();
    QRect res = QGuiApplication::screens()[this->params->getProjectorScreenId()]->geometry();
    this->move(res.x(), res.y());
    this->showFullScreen();
    this->updateView();
}

void PresenterPdf::updateView() {
    QRect projector = QGuiApplication::screens()[this->params->getProjectorScreenId()]->geometry();
    QImage page = this->modele->getImgCurrentPage();
    if (page.width() < projector.width() || page.height() < projector.height()) {
        this->displayPage = QImage(projector.size(), QImage::Format_ARGB32);
        QPainter p(&this->displayPage);
        QPoint pos((projector.width() - page.width()) / 2, (projector.height() - page.height()) / 2);
        p.drawImage(pos, page);
    } else {
        this->displayPage = page;
    }

    this->imgLabel->setPixmap(QPixmap::fromImage(this->displayPage));
}

void PresenterPdf::keyReleaseEvent(QKeyEvent *ev)
{
    if (ev->isAutoRepeat()) {
        ev->ignore();
    } else {
        switch(ev->key())
        {
        case Qt::Key_Escape:
            QCoreApplication::quit();
            break;

        default:
            emit keyPressed(ev);
            break;
        }
    }
}

void PresenterPdf::maybeBlack()
{
    this->isBlack = !this->isBlack;
    if (this->isBlack) {
        this->imgLabel->hide();
    } else {
        this->imgLabel->show();
    }
}
