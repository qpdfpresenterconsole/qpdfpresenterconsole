# QPdfPresenterConsole

This is a software inspired by both Apple's Keynote and Pdf Presenter Console
[http://westhoffswelt.de/projects/pdf_presenter_console.html] written using Qt5.
It is mostly targetting LaTeX/Beamer-generated presentation, with support for
multimedia (audio-only as well as video) and notes.

# System supported

It should work well on at least:
 - Windows 7-10 (not tested for a long time, cross-compilation from Linux should work)
 - macOS 10.13 (not tested for a long time)
 - Linux with XOrg + Xrandr (tested on Ubuntu 19.10)

# Features

 - Presenter-side of the slide deck, with current and next slide, configurable
   timer and out-of-time warning
 - Beamer-generated left/right/top/down notes, displayed on the presenter side
 - Presentation-side with only the current slide
 - Screensaver inhibition during the presentation
 - Audio and video playback (movie15 latex package)

# Building

Building should be fairly easy thanks to CMake:
```
$ git clone ...
$ mkdir build && cd build/
$ cmake ../
$ make -j
```

You should be able to build packages (DEB, RPM) as well using: `$ make package`.

# Dependencies

Building depends on CMake, Qt5, Poppler and libVLC.

In order to compile qpdfpresenterconsole, you will need some qt dependencies:
libpoppler, libwebkit, linguist  You will also need asciidoc, libvlc, and cmake
to compile the sources Those can be brought on Ubuntu 18.04 using: 
```
$ sudo apt install asciidoc cmake qttools5-dev qttools5-dev-tools libpoppler-qt5-dev libqt5webkit5-dev libvlccore-dev
```

# Examples

The repository includes a few PDF examples to demonstrate the abilities.

# Contributions

Any patch are welcome. Send merge request.
