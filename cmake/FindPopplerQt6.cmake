# - Try to find the Qt6 binding of the Poppler library
# Once done this will define
#
#  POPPLER_QT6_FOUND - system has poppler-qt6
#  POPPLER_QT6_INCLUDE_DIR - the poppler-qt6 include directory
#  POPPLER_QT6_LIBRARIES - Link these to use poppler-qt6
#  POPPLER_QT6_DEFINITIONS - Compiler switches required for using poppler-qt6
#

# use pkg-config to get the directories and then use these values
# in the FIND_PATH() and FIND_LIBRARY() calls

# Copyright (c) 2006, Wilfried Huss, <wilfried.huss@gmx.at>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.


find_package(PkgConfig)
pkg_check_modules(PC_POPPLERQT6 QUIET poppler-qt6)

set(POPPLER_QT6_DEFINITIONS ${PC_POPPLERQT6_CFLAGS_OTHER})

find_path(POPPLER_QT6_INCLUDE_DIR
  NAMES poppler-qt6.h
  HINTS ${PC_POPPLERQT6_INCLUDEDIR}
  PATH_SUFFIXES poppler/qt6 poppler
)

find_library(POPPLER_QT6_LIBRARY
  NAMES poppler-qt6
  HINTS ${PC_POPPLERQT6_LIBDIR}
)

set(POPPLER_QT6_LIBRARIES ${POPPLER_QT6_LIBRARY})

if (POPPLER_QT6_INCLUDE_DIR AND POPPLER_QT6_LIBRARIES)
  set(POPPLER_QT6_FOUND TRUE)
else (POPPLER_QT6_INCLUDE_DIR AND POPPLER_QT6_LIBRARIES)
  set(POPPLER_QT6_FOUND FALSE)
endif (POPPLER_QT6_INCLUDE_DIR AND POPPLER_QT6_LIBRARIES)
  
if (POPPLER_QT6_FOUND)
  if (NOT PopplerQt6_FIND_QUIETLY)
    message(STATUS "Found poppler-qt6: library: ${POPPLER_QT6_LIBRARIES}, include path: ${POPPLER_QT6_INCLUDE_DIR}")
  endif (NOT PopplerQt6_FIND_QUIETLY)
else (POPPLER_QT6_FOUND)
  if (PopplerQt6_FIND_REQUIRED)
    message(FATAL_ERROR "Could NOT find poppler-qt6")
  endif (PopplerQt6_FIND_REQUIRED)
endif (POPPLER_QT6_FOUND)
  
mark_as_advanced(POPPLER_QT6_INCLUDE_DIR POPPLER_QT6_LIBRARIES)
