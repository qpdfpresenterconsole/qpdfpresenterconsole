
#---------------------------------#
#      general configuration      #
#---------------------------------#

# version format
version: '{build}-{branch}'

# you can use {branch} name in version format too
# version: 1.0.{build}-{branch}

# branches to build
#branches:
#  # whitelist
#  only:
#    - master
#
#  # blacklist
#  except:
#    - gh-pages

# Maximum number of concurrent jobs for the project
max_jobs: 2

#---------------------------------#
#    environment configuration    #
#---------------------------------#

# Build worker image (VM template)
image:
  - Visual Studio 2019
  - macos-monterey

# fetch repository as zip archive
shallow_clone: true                 # default is "false"

# set clone depth
# clone_depth: 2                      # clone entire repository history if not defined

for:
- matrix:
    only:
      - image: Visual Studio 2019
  # exclude configuration from the matrix. Works similarly to 'allow_failures' but build not even being started for excluded combination.
    exclude:
      - platform: x86
        configuration: Debug

  #---------------------------------#
  #       build configuration       #
  #---------------------------------#

  # build platform, i.e. x86, x64, Any CPU. This setting is optional.
  platform: x64

  # to add several platforms to build matrix:
  #platform:
  #  - x86
  #  - Any CPU

  # build Configuration, i.e. Debug, Release, etc.
  configuration: Release

  # to add several configurations to build matrix:
  #configuration:
  #  - Debug
  #  - Release

  # clone directory
  clone_folder: c:\projects\qpdfpresenterconsole

  cache:
    - C:\Tools\vcpkg\installed
    - C:\projects\qpdfpresenterconsole\poppler-22.05.0\build

  # scripts that run after cloning repository
  install:
    - ps: |
        pwd
        wget https://mirrors.ircam.fr/pub/videolan/vlc/3.0.17/win64/vlc-3.0.17-win64.7z -OutFile vlc-3.0.17-win64.7z
        7z x vlc-3.0.17-win64.7z
        ls vlc-3.0.17/sdk/
        C:\Tools\vcpkg\vcpkg install freetype:x64-windows devil:x64-windows openjpeg:x64-windows libiconv:x64-windows
        wget https://poppler.freedesktop.org/poppler-22.05.0.tar.xz -OutFile poppler-22.05.0.tar.xz
        7z x poppler-22.05.0.tar.xz; 7z x poppler-22.05.0.tar
        cd poppler-22.05.0
        mkdir -Force build
        ls build
        cd build
        cmake ../ -DCMAKE_PREFIX_PATH="C:/Qt/6.3.0/msvc2019_64/lib/cmake/" -DCMAKE_TOOLCHAIN_FILE=C:/Tools/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_BUILD_TYPE=release -DWITH_NSS3=OFF -DENABLE_BOOST=OFF -DPROJECT_PACK_BIN=OFF -DENABLE_QT5=OFF -DENABLE_QT6=ON
        MSBuild -p:Configuration=Release .\qt6\src\poppler-qt6.vcxproj
    - sh: brew list

  # scripts to run before build
  before_build:
    - ps: |
        cd C:/projects/qpdfpresenterconsole
        pwd
        mkdir build
        cd build
        cmake ../ -DCMAKE_PREFIX_PATH=C:/Qt/6.3.0/msvc2019_64/lib/cmake/ -DPOPPLER_QT6_LIBRARY=C:/projects/qpdfpresenterconsole/poppler-22.05.0/build/qt6/src/Release/poppler-qt6.lib -DPOPPLER_QT6_INCLUDE_DIR="C:/projects/qpdfpresenterconsole/poppler-22.05.0/qt6/src/;C:/projects/qpdfpresenterconsole/poppler-22.05.0/build/qt6/src/" -DLIBVLC_INCLUDE_DIR=C:/projects/qpdfpresenterconsole/vlc-3.0.17/sdk/include/ -DLIBVLC_LIBRARY=C:/projects/qpdfpresenterconsole/vlc-3.0.17/sdk/lib/libvlc.lib -DLIBVLCCORE_LIBRARY=C:/projects/qpdfpresenterconsole/vlc-3.0.17/sdk/lib/libvlccore.lib -DWINQTDIR=C:/Qt/6.3.0/msvc2019_64/ -DWINPOPPLERDIR=C:/projects/qpdfpresenterconsole/poppler-22.05.0/build/qt6/src/Release/ -DWINLIBVLCDIR=C:/projects/qpdfpresenterconsole/vlc-3.0.17/ -DWINVCPKGDIR=C:/Tools/vcpkg/installed/x64-windows/ -DUSE_QT6=1

  # to run your custom scripts instead of automatic MSBuild
  build_script:
    - ps: |
        cd C:/projects/qpdfpresenterconsole/build
        pwd
        MSBuild -p:Configuration=Release qpdfpresenterconsole.vcxproj
        cpack

  # scripts to run after build (working directory and environment changes are persisted from the previous steps)
  after_build:

  # scripts to run *after* solution is built and *before* automatic packaging occurs (web apps, NuGet packages, Azure Cloud Services)
  before_package:

  artifacts:
    - path: build\QPdfPresenterConsole-*.exe
      name: 'Windows x64 Installer'

    - path: build\QPdfPresenterConsole-*.zip
      name: 'Windows x64 Zip package'

- matrix:
    only:
      - image: macos-monterey

  # clone directory
  clone_folder: /Users/appveyor/projects/qpdfpresenterconsole

  cache:
    - /Users/appveyor/Library/Caches/Homebrew

  environment:
    PKG_CONFIG_PATH: "/usr/local/opt/poppler/lib/pkgconfig"
    XML_CATALOG_FILES: "/usr/local/etc/xml/catalog"

  # scripts that run after cloning repository
  install:
    - sh: |
        pwd
        rm '/usr/local/include/X11'
        brew update
        brew install pkgconfig qt6 poppler vlc asciidoc

  # scripts to run before build
  before_build:
    - sh: |
        pwd
        mkdir build
        cd build
        cmake ../ -DVLC_PATH=/Applications/VLC.app -DCMAKE_PREFIX_PATH=/usr/local/opt/qt6/lib/cmake/ -DBROTLI_DIR=/usr/local/opt/brotli -DUSE_QT6=1

  # to run your custom scripts instead of automatic MSBuild
  build_script:
    - sh: |
        pwd
        make -j$(nproc)
        cpack -G DragNDrop
        cpack -G ZIP
        rm -fr _CPack_Packages/

  # scripts to run after build (working directory and environment changes are persisted from the previous steps)
  after_build:

  # scripts to run *after* solution is built and *before* automatic packaging occurs (web apps, NuGet packages, Azure Cloud Services)
  before_package:

  artifacts:
    - path: build\QPdfPresenterConsole-*.dmg
      name: 'macOS x64 DMG'

    - path: build\QPdfPresenterConsole-*.zip
      name: 'macOS x64 Zip package'

